import SingleBase from '../base/SingleBase';
import {event_base} from '../../config/ClientEvent';

export default class EventEmitter extends SingleBase {
	emitEvent(event: event_base, is_later: boolean = true) {
	    event.name = event.getName();
		if (is_later) {
		    this.timer().then(() => {
                cc.systemEvent.emit(event.name, event);
            })
        }
		else {
            cc.systemEvent.emit(event.name, event);
        }
	}
	registerEvent(event: event_base | string, callfunc: Function, target?: any) {
		if (typeof event === "string") {
		    cc.systemEvent.on(event, callfunc, target);
        }
		else {
            cc.systemEvent.on(event.name, callfunc, target);
        }
	}
	unregisterEvent(event: event_base | string, callfunc: Function, target?: any) {
		if (typeof event === "string") cc.systemEvent.off(event, callfunc, target);
		else cc.systemEvent.off(event.name, callfunc, target);
	}
}