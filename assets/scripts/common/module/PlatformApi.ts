import SingleBase from '../base/SingleBase';
import TtApi from './platform/TtApi';
import KwaiApi from './platform/KwaiApi';
import { event_base } from '../../config/ClientEvent';
import WxApi from './platform/WxApi';

let platform: PlatformApi;

export default class PlatformApi extends SingleBase {
    onLoad() {
        if (window.wx && false) {
            platform = WxApi.get();
        } else if (window.tt) {
            platform = TtApi.get();
        } else if (window.ks) {
            platform = KwaiApi.get();
        }
        if (platform) {
            platform.onLoad();
            platform.showShareMenu();
        }
    }

    /*提示弹层*/
    showToast(title: string, icon: string = 'none', duration: number = 2000) {
        if (!platform) {
            console.log('-------------：', title);
            return;
        }
        platform.showToast(title, icon, duration);
    }

    /*开始录屏*/
    startRecordVideo(duration: number = 90) {
        if (!platform) {
            return;
        }
        platform.startRecordVideo(duration);
    }

    /*停止录屏*/
    stopRecordVideo(cb?: Function) {
        if (!platform) {
            return;
        }
        platform.stopRecordVideo(cb);
    }

    /*显示视频广告*/
    showVideoAd(callback: Function, is_share: boolean = true) {
        // console.log('-----显示视频广告--------', cc.sys.platform === cc.sys.BYTEDANCE_GAME , tt.env.VERSION)tt.env.VERSION == "preview"
        // if (cc.sys.isBrowser ) {
        //     // if(cc.sys.isBrowser || (cc.sys.platform === cc.sys.BYTEDANCE_GAME && tt.env.VERSION == "preview")) {
        //         return;
        //     }
        if (!platform) {
            callback();
            return;
        }
        platform.showVideoAd(callback, is_share);
    }

    /*加载视频广告*/
    loadVideoAd() {
        if (!platform) {
            return;
        }
        platform.loadVideoAd();
    }

    /*重新加载视频广告*/
    reLoadVideo() {
        if (!platform) {
            return;
        }
        platform.reLoadVideo();
    }

    /*右上角分享*/
    showShareMenu() {
        if (!platform) {
            return;
        }
        platform.showShareMenu();
    }

    /*主动分享*/
    onShareAppMessage(callback: Function, is_reward: boolean = true) {
        if (!platform) {
            return;
        }
        platform.onShareAppMessage(callback, is_reward);
    }

    /*被动分享*/
    onShareAppVideo(callback: Function, is_reward: boolean = false) {
        if (cc.sys.isBrowser) {
            callback && callback();
            return;
        }
        if (!platform) {
            return;
        }
        platform.onShareAppVideo(callback, is_reward);
    }

    /*关注*/
    openAwemeUserProfile(callback: Function) {
        if (cc.sys.isBrowser) {
            callback && callback();
            return;
        }
        if (!platform) {
            return;
        }
        platform.openAwemeUserProfile(callback);
    }

    /*是否有记录视频*/
    isRecordVideo() {
        if (!platform) {
            return false;
        }
        platform.isRecordVideo();
        return true;
    }

    isPlatform(): boolean {
        if (!platform) {
            return;
        }
        return platform.isPlatform();
    }

    setFrameRate(fps: number) {
        if (!platform) {
            return;
        }
        platform.setFrameRate(fps);
    }

    loadRecordVideo(res) {
        platform.loadRecordVideo(res);
    }

    reportAnalytics(name: string, data: any = {}) {
        if (!platform) {
            return;
        }
        platform.reportAnalytics(name, data);
    }

    createInterstitialAd() {
        if (!window.tt) {
            return;
        }
        platform.createInterstitialAd();
    }

    showInterstitialAd() {
        if (!window.tt) {
            return;
        }
        platform.showInterstitialAd();
    }
}