export default class UITools {
    static alignFullScreen(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.left = 0;
        widget.right = 0;
        widget.bottom = 0;
        widget.top = 0;
        widget.isAlignTop = true;
        widget.isAlignBottom = true;
        widget.isAlignLeft = true;
        widget.isAlignRight = true;
    }

    static alignCenter(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.isAlignHorizontalCenter = true;
        widget.isAlignVerticalCenter = true;
        widget.horizontalCenter = 0;
        widget.verticalCenter = 0;
    }

    static alignLeft(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.left = 0;
        widget.isAlignLeft = true;
    }

    static alignRight(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.right = 0;
        widget.isAlignRight = true;
    }

    static alignTop(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.top = 0;
        widget.isAlignTop = true;
    }

    static alignBottom(node: cc.Node, mode: number) {
        let widget: cc.Widget = node.findComponent(cc.Widget);
        widget.alignMode = mode;
        widget.bottom = 0;
        widget.isAlignBottom = true;
    }

    static easingScale(node: cc.Node) {
        let scale = node.scale;
        node.scale = 0;
        node.stopAllActions();
        cc.tween(node)
            .to(0.1, {scale: scale + 0.15})
            .to(0.2, {scale: scale})
            .start();
    }

    static easingFall(node: cc.Node) {
        let y = node.y;
        node.y = cc.winSize.height;
        node.x = 0;
        node.stopAllActions();
        cc.tween(node)
            .by(0.3, {position: cc.v2(0, -cc.winSize.height - 20 + y)}, {easing: cc.Tween.expoOut})
            .by(0.2, {position: cc.v2(0, 20)})
            .start();
    }

    static easingFade(node: cc.Node) {
        node.opacity = 0;
        node.stopAllActions();
        cc.tween(node)
            .to(0.1, {opacity: 150}, {easing: cc.Tween.sineIn})
            .to(0.1, {opacity: 255}, {easing: cc.Tween.sineOut})
            .start();
    }

    static removeAllNode(arr: cc.Node[]) {
        while(arr[0]) {
            arr[0].destroy();
            arr.shift();
        }
    }

    static easingRise(node: cc.Node) {
        let y = node.y;
        node.y = -cc.winSize.height;
        node.x = 0;
        node.stopAllActions();
        cc.tween(node)
            .by(0.3, {position: cc.v2(0, cc.winSize.height + 20 + y)}, {easing: cc.Tween.expoOut})
            .by(0.2, {position: cc.v2(0, -20)})
            .start();
    }

    /*呼吸效果*/
    static runBreathingAction(node: cc.Node) {
        node.stopAllActions();
        let scale = node.scale;
        cc.tween(node)
            .to(0.5, {scale: scale + 0.1})
            .to(0.5, {scale})
            .union()
            .repeatForever()
            .start();
    }

    /*软弹簧效果*/
    static runBounceAction(node: cc.Node) {
        node.stopAllActions();
        let scaleX = node.scaleX;
        let scaleY = node.scaleY;
        cc.tween(node)
            .to(0.15, {scaleX: scaleX * 0.5, scaleY: scaleY * 1.2}, {easing: 'bounceInOut'})
            .to(0.15, {scaleX: scaleX * 1.3, scaleY: scaleY * 0.8}, {easing: 'bounceInOut'})
            .to(0.2, {scaleX: scaleX * 0.9, scaleY: scaleY}, {easing: 'smooth'})
            .delay(0.7)
            .union()
            .repeatForever()
            .start()
    }

    /*震动屏幕*/
    static shakeScreen(node: cc.Node) {
        if(node.is_tween) {
            return;
        }
        node.is_tween = true;
        let delay = 1;
        cc.tween(node)
            .by(0.1, {y: 5, x: 5})
            .by(0.1, {y: -5, x: -5})
            .delay(delay)
            .call(() => {
                node.is_tween = false;
            })
            .union()
            .start();
    }
    /*伤害颜色*/
    static _hurtColor(node: cc.Node) {
        cc.tween(node)
            .to(0.2, {color: cc.color(255, 0, 0)})
            .to(0.2, {color: cc.color(255, 255, 255)})
            .start();
    }

    
}