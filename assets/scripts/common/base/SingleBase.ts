export default class SingleBase {
    private static _instance;
    static get<T>(this: new ()=> T): T {
        if(!(<any>this)._instance){
            (<any>this)._instance = new this();
        }
        return (<any>this)._instance;
    }
    timer() {
        return new Promise((resolve, reject) => {
            resolve(reject);
        })
    }
}