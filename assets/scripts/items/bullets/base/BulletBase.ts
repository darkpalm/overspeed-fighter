// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { LQCollide } from "../../../../lq_collide_system/lq_collide";
import { $, cNodePool, cSound, gameData } from "../../../config/Config";
import { gameScene } from "../../../view/scene/GameScene";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletBase extends LQCollide {
    @property(cc.ParticleSystem)
    particle: cc.ParticleSystem = null;
    _spr: cc.Sprite = undefined;
    _penetrate_count: number = 0;
    _max_penetrate_count: number = 0;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._spr = this.node.getChildByName("spr").getComponent(cc.Sprite);
    }

    start() {
        
    }

    /**
     * 向某一个点射击
     * @param pos 
     */
    shootToPos(pos: cc.Vec2, time: number) {
        this._penetrate_count = 0;
        this._spr.node.opacity = 255;
        this.is_enable = true;
        this.particle.resetSystem();
        cc.tween(this.node)
        .to(time, {position: pos})
        .call(()=>{
            this.removeSelf();
        })
        .start()

        this.initData();
    }

    initData(){
        this._hurt = 1 + Math.floor(gameData._enemy_wave/5);
        this.node.scale = 1; 
        this.radius = this.node.width/2;
        this._max_penetrate_count = 0;
    }

    // @ts-ignore
    public on_collide(collide: LQCollide): void {
    }

    //@ts-ignore
    public on_enter(collide: LQCollide) {
        this._penetrate_count++;
        if(this._penetrate_count > this._max_penetrate_count || collide._key == "boss") this.removeSelf();
    }

    //@ts-ignore
    public on_exit(collide: LQCollide) {
    }

    /**移除自身 */
    removeSelf(){
        this._spr.node.opacity = 0;
        this.node.stopAllActions();
        this.particle.stopSystem();
        this.is_enable = false;
        this.scheduleOnce(()=>{
            cNodePool.put(this.node);
        }, 0.3)
    }

    // update (dt) {}
}
