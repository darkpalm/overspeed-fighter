import { gameScene } from "../../view/scene/GameScene";
import BulletBase from "./base/BulletBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class bullet extends BulletBase {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    initData(){
        this._penetrate_count = 0;
        this._hurt = gameScene.heroCtrl._hurt;
        this.node.scale = gameScene.heroCtrl._bullet_volume; 
        this.radius = this.node.width*gameScene.heroCtrl._bullet_volume/2;
        this._max_penetrate_count = gameScene.heroCtrl._penetrate_count;
    }

    // update (dt) {}
}
