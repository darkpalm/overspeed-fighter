// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { LQCollide } from "../../../lq_collide_system/lq_collide";
import { $, cNodePool, cSound, gameData } from "../../config/Config";
import { p_data_mager } from "../../Mangers/playerDataMager/PlayerDataMager";
import { gameScene } from "../../view/scene/GameScene";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Xingxing extends LQCollide {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.is_enable = false;
    }

    start() {

    }

    initPos(pos: cc.Vec2) {
        this.is_enable = false;
        this.node.position = pos;

    }


    moveToPlayer: boolean = false;
    move() {
        this.moveToPlayer = true;
        this.is_enable = true;
    }

    update(dt) {
        if (this.moveToPlayer) {

            this.Genzong();
        }
    }

    Genzong() {
        let targetPoint = gameScene.heroCtrl.getPos();
        let _delta = targetPoint.sub(this.node.position);
        _delta.normalizeSelf();
        var x2 = this.node.x + 20 * _delta.x;
        var y2 = this.node.y + 20 * _delta.y;
        this.node.setPosition(cc.v2(x2, y2));//设置跟踪的位置
    }

    // @ts-ignore
    public on_collide(collide: LQCollide): void {
    }

    //@ts-ignore
    public on_enter(collide: LQCollide) {
        this.is_enable = false;
        this.moveToPlayer = false;
        cNodePool.put(this.node);
        gameData._xing_guang_count++;
        p_data_mager.playerData.xinPianSuiPian++;
        gameScene.PropRoot.judgeFinish();
        gameScene.GameUI.updateStar();
        cSound.playEffect($.Sound.pickup);
    }

    //@ts-ignore
    public on_exit(collide: LQCollide) {
    }
}
