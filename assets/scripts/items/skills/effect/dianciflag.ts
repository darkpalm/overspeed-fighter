// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../../../common/utils/Utils";
import { cNodePool, gameData } from "../../../config/Config";
import { gameScene } from "../../../view/scene/GameScene";

const {ccclass, property} = cc._decorator;

@ccclass
export default class dianciflag extends cc.Component {

    endTime: number = 0;

    initTime(time: number){
        this.endTime = gameData._game_milliscoend + time;
        this.node.width = this.node.parent.width*1.3;
        this.node.height = this.node.parent.height*1.3;
        let opacity = Utils.getNumMinToMax(150,255);
        this.node.stopAllActions();
        cc.tween(this.node)
        .to(1, {opacity: 120, scale: 1.1})
        .to(1, {opacity: opacity, scale: 1})
        .union()
        .repeatForever()
        .start()
    }

    update (dt) {
        if(this.endTime <= gameData._game_milliscoend){
            this.node.removeFromParent();
            cNodePool.put(this.node);
        }
    }
}
