// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../../common/utils/Utils";
import { $, cNodePool, cSound, gameData } from "../../config/Config";
import { gameScene } from "../../view/scene/GameScene";
import ShowBoss from "../pars/ShowBoss";
import EnemyBase from "./EnemyBase";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BossBase extends EnemyBase {

    move_index: number = 0;
    follow_time: number = 300;
    move_to_follow: boolean = false;
    follow_time_flag: number = 0;

    isShooting: boolean = false;

    shoot_time: number = 480;
    shoot_time_cd: number = 0;
    shoot_count: number = 18;


    // onLoad () {}

    start() {

    }

    onEnable(): void {
        cSound.playMusic($.Sound.bossBgm, 3);
        this.shoot_count = 18 + Math.floor(gameData._enemy_wave / 4) * 6;
        this._canHitBack = false;
        this._is_frozen = false;
        this.isShooting = false;
        this._add_move_speed = 0;
        this._sub_move_speed = 0;
        // this._isBackAction = false;
        this._HP = gameData._enemy_wave * 10 + 10;
        this._speed = gameScene.heroCtrl._speed;
        this._hurt = 1 + Math.floor(gameData._enemy_wave / 4);
        if (this._hurt > 5) this._hurt = 5;

        this.follow_time = this.follow_time + Utils.getNumMinToMax(0, gameData._enemy_wave * 60);
        this.initShootTime();

        gameScene.bossHpRoot.node.active = true;
        gameScene.bossHpRoot.initData(this._HP);
    }

    initData() {
        this._key = "boss";
        this.is_enable = false;
        this.node.opacity = 0;
        let position = Utils.getRadomPos(gameScene.heroCtrl.getPos(), 2.4);
        cNodePool.get($.Prefab.showBoss).then(value => {
            value.parent = this.node.parent;
            value.getComponent(ShowBoss).initState(this._key, position);
        })
        this.node.position = position;
        cc.tween(this.node)
            .to(3, { opacity: 255 })
            .call(() => {
                this.is_enable = true;
                this.moveToPos();
            })
            .start();
    }

    //@ts-ignore
    public on_enter(collide: LQCollide) {
        // this.hitBackAction(collide.node.position);
        // return;
        if (collide.collide_category == 2) {
            if (!this.hurtCdTime()) return;
            this._HP -= 2;
            if (this._HP <= 0) {
                this._HP = 0;
                this.removeSelf();
            } else {
                this.hitBackAction(collide.node.position);
            }
            this.moveFarHero();
        } else {
            if (!this.hurtCdTime()) return;
            this._HP -= collide._hurt;
            if (this._HP <= 0) {
                this._HP = 0;
                if (collide.collide_category == 4) if (gameScene.heroCtrl._life_stealing > 0) {
                    let value = gameScene.heroCtrl._hurt * gameScene.heroCtrl._life_stealing;
                    gameScene.heroCtrl.addBlood(value);
                }
                this.judgeProp();
                gameData._kill_enemy_count++;
                this.removeSelf();
            } else {
                this.hitBackAction(collide.node.position);
                if (collide.collide_category == 4) this.frozenMoveSpeed();
            }
        }

        gameScene.bossHpRoot.updateBossPro(this._HP);
    }

    initShootTime() {
        this.shoot_time = 480 + Utils.getNumMinToMax(-Math.floor(gameData._enemy_wave / 5) * 120, 180);
    }

    update(dt) {
        if (gameScene.is_pause || this.isShooting || !this.is_enable) return;
        if (this.node.getChildByName("dianciflag")) return;
        this.Genzong();
        this.shoot_time_cd++;
        if (this.shoot_time_cd >= this.shoot_time) {
            this.shoot_time_cd = 0;
            this.shootBullet(this.shoot_count);
        }
    }

    Genzong() {
        if (this.move_to_follow) {
            this.follow_time_flag++;
            if (this.follow_time_flag >= this.follow_time) {
                this.move_to_follow = false;
                this.follow_time_flag = 0;
            }
            this._delta = gameScene.heroCtrl.node.position.sub(this.node.position);
            if (this._delta.mag() <= 5) {
                this.move_to_follow = false;
                this.follow_time_flag = 0;
                this.moveToPos();
            }
        } else {
            this._delta = this._target_pos.sub(this.node.position);
            if (this._delta.mag() <= 5) {
                this.moveToPos();
            }
        }
        this._delta.normalizeSelf();
        let speed = this._speed + this._add_move_speed + this._sub_move_speed;
        var x2 = this.node.x + speed * this._delta.x;
        var y2 = this.node.y + speed * this._delta.y;
        this.node.setPosition(cc.v2(x2, y2));//设置跟踪的位置
        this.node.angle += (2 + this._add_move_speed / 2);
    }

    moveToPos() {
        this.move_index++;
        if (this.move_index % 5 == 0) {
            this.move_to_follow = true;
            return;
        }

        let flag = gameData._enemy_wave * 0.1 + 0.1;
        if (flag > 0.65) flag = 0.65;
        if (Math.random() * 1 < flag) {
            this._add_move_speed = 2;
            this._target_pos = Utils.getRadomPos(gameScene.heroCtrl.getPos(), 9, true);
            return;
        } else {
            this._add_move_speed = 0;
        }
        this._target_pos = Utils.getRadomPos(gameScene.heroCtrl.getPos(), 2.2, true);


    }

    moveFarHero() {
        this.move_to_follow = false;
        this.follow_time_flag = 0;
        this._add_move_speed = 3;
        this._target_pos = Utils.getRadomPos(gameScene.heroCtrl.getPos(), 2.2, true);
    }


    shootBullet(count: number = 12) {
        this.isShooting = true;
        let angleFlag = 360 / count;
        let index = 0;
        this.schedule(() => {
            let delat = Utils.angle_to_vector(angleFlag * index);
            let pos = cc.v2(this.node.x + delat.x * 1000, this.node.y + delat.y * 1000);
            gameScene.bossShootBullet(pos, this.node.position);
            index++;
            if (index == count) {
                this.isShooting = false;
            }
        }, 0.1, count - 1);
    }

    removeSelf() {
        cSound.playEffect($.Sound.boss_death);
        let pos = this.node.position;
        cNodePool.get($.Prefab.boss).then(value => {
            value.parent = gameScene.node;
            value.position = pos;
        })
        this.node.removeFromParent();
        this.node.destroy();
        gameScene.startCheckEnemyCount();
        cSound.playMusic($.Sound.game_bgm, 1);
        gameScene.bossHpRoot.node.active = false;
    }

    judgeProp() {
        return;
        let ra = 0.2 + Math.random() * (10 - gameData._enemy_wave) * 0.05;
        if (ra < 0.1) ra = 0.1;
        ra += gameScene.heroCtrl._lucky;
        if (Math.random() * 1 < ra) {
            gameScene.PropRoot.propXingXing(this.node.position);
        }
    }

    hitBackAction(pos: cc.Vec2) {
        cSound.playEffect($.Sound.boss_hit);

        this.node.stopAllActions();
        cc.tween(this.node)
            .to(0.2, { opacity: 120 })
            .to(0.2, { opacity: 255 })
            .start()
        cc.tween(this._sprNode)
            .to(0.2, { color: new cc.Color(255, 0, 0) })
            .to(0.2, { color: this._color })
            .start()

        this._isBackAction = true;
        let dir = pos.sub(this.node.position);
        dir.normalizeSelf();
        let endPos = cc.v2(this.node.x - 20 * dir.x, this.node.y - 20 * dir.y);
        cc.tween(this.node)
            .to(0.1, { position: endPos })
            .call(() => {
                this._isBackAction = false;
            })
            .start()
    }

    // update (dt) {}
}
