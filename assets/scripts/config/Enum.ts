
export enum E_State {
    Idle,
    Walk,
    Flee,
    Evade,
    Attack,
    Dead,
    Pursuit,
}

export enum E_Pursuit {
    Yes,
    No
}

export enum E_Walk {
    Random,
    Normal,
}

export enum E_Draw_Path {
    Yes,
    No,
}

export enum E_Game_Fail {
    Fail_1,
    Fail_2,
    Fail_3
}
export enum E_Destroy_Self {
    Yes,
    No,
}
export enum E_Follow_Target {
    Yes,
    No,
}
export enum E_Collision_Effect {
    Remove,
    Inhalation
}

export enum E_Item_Motion {
    /*静止*/
    Still,
    /*圆周*/
    Circle,
    /*椭圆*/
    Ellipse,
    /*固定路线*/
    Route,
    /*重力*/
    Gravity,
    /*随机位置*/
    RandomPos,
}