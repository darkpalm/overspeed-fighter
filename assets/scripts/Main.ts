/*启动入口*/
import ccclass = cc._decorator.ccclass;
import { $, cStorage, cUI, pfApi } from './config/Config';
import { p_data_mager } from './Mangers/playerDataMager/PlayerDataMager';
import { HttpRequest } from './common/utils/HttpRequest';

@ccclass
export default class Game extends cc.Component {
    is_console_log: boolean = true;
    frame_rate: number = 60;
    physics = {
        enabled: true,
        debug: false,
        gravity: -640
    };
    onLoad() {
        //强制开启动态合图
        cc.macro.CLEANUP_IMAGE_CACHE = false;
        cc.dynamicAtlasManager.enabled = true;
        //强制禁用动态合图
        cc.dynamicAtlasManager.enabled = false;

        HttpRequest.registerHeadImgLoader();

        p_data_mager.initData();
        // this.onLoadConfig();
        pfApi.onLoad();
    }
    start() {
        if(window.wx  && false){
            let self = this;
            const loadTask = wx.loadSubpackage({
                name: 'common', // 下载其他分包
                success(res) {
                    cc.assetManager.loadBundle('common', (err, bundle) => {
                        if (err) {
                            pfApi.showToast("加载资源失败，请重启！");
                        } else {
                            self.startGame();
                        }
                    });
                  console.log('load 下载其他分包 success')
                },
                fail(err) {
                  console.error('load moduleA fail', err)
                }
              })
        }else{
            this.startGame();
        }
    }
    
    startGame(){
        cUI.addScene($.Prefab.main_scene).then(value => {
            // if (!cc.sys.isBrowser) {
            //     cUI.addLayer($.Prefab.notice, false);
            // }
            this.node.getChildByName("bg").destroy();
        });
    }

    onLoadConfig() {
        if (!this.is_console_log) {
            console.log = function () {
            };
            console.warn = function () {
            };
            console.error = function () {
            };
        } else {
            cc.log = function (val: any) {
                if (val instanceof cc.Vec2) {
                    console.log(val.x, val.y);
                } else if (val instanceof cc.Vec3) {
                    console.log(val.x, val.y, val.z);
                }
            }
        }

        if (pfApi.isPlatform()) {
            pfApi.setFrameRate(this.frame_rate);
        } else {
            cc.game.setFrameRate(this.frame_rate);
        }
        let physics = cc.director.getPhysicsManager();
        physics.enabled = this.physics.enabled;
        let DrawBits = cc.PhysicsManager.DrawBits;
        if (this.physics.debug) {
            physics.debugDrawFlags = DrawBits.e_shapeBit;
        }
        physics.gravity = cc.v2(0, this.physics.gravity);
        // physics.enabledAccumulator = true;
        // physics['FIXED_TIME_STEP'] = 1 / this.frame_rate;

    }
}