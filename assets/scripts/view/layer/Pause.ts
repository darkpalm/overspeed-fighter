import {PauseView} from '../base/ViewSet';
import {$, cSound, cUI, gameData, pfApi} from '../../config/Config';
import {gameScene} from '../scene/GameScene';
import UITools from '../../common/utils/UITools';
import { p_data_mager } from '../../Mangers/playerDataMager/PlayerDataMager';

const {ccclass, property} = cc._decorator;

@ccclass
export default class Pause extends PauseView {
    @property(cc.Toggle)
    ziDongSheJi: cc.Toggle = undefined;
    @property(cc.Toggle)
    musicToggle: cc.Toggle = undefined;
    @property(cc.Toggle)
    effectToggle: cc.Toggle = undefined;
    @property(cc.Node)
    attRoot: cc.Node = undefined;
    @property(cc.Label)
    nowKillEmenyCount: cc.Label = undefined;
    @property(cc.Label)
    nowLifeDay: cc.Label = undefined;
    @property(cc.Label)
    recordKillEmenyCount: cc.Label = undefined;
    @property(cc.Label)
    recordLifeDay: cc.Label = undefined;
    @property(cc.Node)
    confirmNode: cc.Node = undefined;
    @property(cc.Node)
    btn_continue: cc.Node = undefined;

    protected onLoad(): void {
        UITools.runBreathingAction(this.btn_continue);

        this.ziDongSheJi.isChecked = gameData._auto_shoot;
        this.musicToggle.isChecked = cSound.music_switch;
        this.effectToggle.isChecked = cSound.effect_switch;
        this.showAtt();
        this.showRecord();
        this.confirmNode.active = false;
    }

    changeConfirmNode(){
        cSound.playEffect($.Sound.btn_0);
        this.confirmNode.active = !this.confirmNode.active;
    }

    onBtnContinue(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        gameScene.resumeGame();
    }

    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        cUI.addScene($.Prefab.main_scene);
        pfApi.stopRecordVideo();
    }

    changeAutoShoot(){
        cSound.playEffect($.Sound.btn_0);
        gameData._auto_shoot = !gameData._auto_shoot;
        cUI.showTips(gameData._auto_shoot ? "开启全自动射击" : "关闭全自动射击", 1000);
        if(gameData._auto_shoot){
            gameScene.heroCtrl.startShoot();
        }else{
            gameScene.heroCtrl.stopShoot();
        }
    }

    musicSwitch(){
        cSound.playEffect($.Sound.btn_0);
        cSound.toggleMusic();
        this.musicToggle.isChecked = cSound.music_switch;
    }

    effectSwitch(){
        cSound.toggleEffect();
        cSound.playEffect($.Sound.btn_0);
        this.effectToggle.isChecked = cSound.effect_switch;
    }

    showAtt(){
        this.attRoot.children.forEach(value =>{
            value.getComponent(cc.Label).string = gameScene.heroCtrl[value.name];
        })
    }

    showRecord(){
        this.nowKillEmenyCount.string = gameData._kill_enemy_count.toString();
        this.nowLifeDay.string = gameData._enemy_wave.toString();
        this.recordKillEmenyCount.string = p_data_mager.playerData.maxKillCount.toString();
        this.recordLifeDay.string = p_data_mager.playerData.maxLifeDay.toString();
    }

}