import {NoticeView} from '../base/ViewSet';
import {$, cSound, cStorage, cUI, pfApi} from '../../config/Config';

const {ccclass, property} = cc._decorator;

@ccclass
export default class Notice extends NoticeView {
    protected onLoad(): void {
    }

    onEnable(){
        this.node.$("mask").show();
    }

    closeAnim() {
        let offset_x  = (cc.winSize.width / cc.winSize.height - 1.77);
        cc.tween(this.node)
            .to(0.2, {scale: 0, position: cc.v2(-295 -295 * offset_x, 321)}, {easing: 'sineOut'})
            .call(()=> {
                cUI.removeLayer(this.node);
            })
            .start();
    }
    onBtnClose(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        this.node.$("mask").hide();
        this.closeAnim();
    }
    // update (dt) {}
}
