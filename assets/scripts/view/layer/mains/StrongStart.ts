
import { HttpRequest } from '../../../common/utils/HttpRequest';
import UITools from '../../../common/utils/UITools';
import { event_strong_start } from '../../../config/ClientEvent';
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import { p_data_mager } from '../../../Mangers/playerDataMager/PlayerDataMager';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const { ccclass, property } = cc._decorator;

@ccclass
export default class StrongStart extends ViewBase {


    protected onLoad(): void {
        UITools.runBreathingAction(this.node.getChildByName("bg1").getChildByName("_btn_attNode"));
    }


    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        event_strong_start.emitIndex(0);
    }

    onBtnAttNode(e: cc.Event.EventTouch, data: string) {
        cSound.playEffect($.Sound.btn_0);
        pfApi.showVideoAd(this.videoFinish.bind(this));
    }
    
    videoFinish(){
        cUI.removeLayer(this.node);
        event_strong_start.emitIndex(1);
        gameData._is_strong_start = true;
        pfApi.reportAnalytics('strongStart', {});
    }

}