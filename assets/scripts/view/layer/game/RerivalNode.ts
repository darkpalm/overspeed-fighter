
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const {ccclass, property} = cc._decorator;

@ccclass
export default class RerivalNode extends ViewBase {


    protected onLoad(): void {
        // gameData._last_game_day = gameData._enemy_wave;
    }


    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        gameScene.gameResult();
    }

    onBtnRevival(){
        cSound.playEffect($.Sound.btn_0);
        pfApi.showVideoAd(this.onReriral.bind(this));
    }

    onReriral(){
        cUI.removeLayer(this.node);
        gameScene.heroCtrl.rerivalAction();

    }

}