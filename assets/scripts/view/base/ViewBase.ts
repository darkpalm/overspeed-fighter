import UITools from '../../common/utils/UITools';
import AutoReleaseAssets from '../../compoment/AutoReleaseAssets';
import {event_base} from '../../config/ClientEvent';
import {cEvent} from '../../config/Config';

enum BlockInput {
    on,
    off,
}

enum AlignMode {
    none,
    center,
    full,
    left,
    right,
    top,
    bottom
}

enum ShowMode {
    none,
    easingScale,
    easingFade,
    easingFall,
    easingRise,
    random,
}

const {ccclass, property} = cc._decorator;
@ccclass
export default class ViewBase extends AutoReleaseAssets {
    @property({type: cc.Enum(BlockInput)})
    private blockInputEvent = BlockInput.off;
    @property({type: cc.Enum(AlignMode)})
    private alignMode = AlignMode.none;
    @property({type: cc.Enum(ShowMode)})
    private showMode = ShowMode.none;
    _events: Array<{ event: event_base, callfunc: Function, target: any }> = [];
    open_type: {};
    protected onEnable(): void {
        this.initEvents();
        this.addBlockInput();
    }

    protected onDisable(): void {
        this.uninitEvents();
    }

    // protected onDestroy(): void {

    // }

    protected initEvents() {

    }

    /**/

    /*ws服务器连接成功 仅场景中有效*/
    onConnectSuccess() {

    }
    close() {

    }
    protected uninitEvents() {
        this._events.forEach(value => {
            this.unregisterEvent(value.event, value.callfunc, value.target);
        });
        this._events = [];
    }

    protected registerEvent(event: event_base, callfunc: Function, target?: any) {
        event = new event();
        event.name = event.getName();
        this._events.push({event, callfunc, target});
        cEvent.registerEvent(event, callfunc, target);
    }

    protected unregisterEvent(event: event_base, callfunc: Function, target?: any) {
        cEvent.unregisterEvent(event, callfunc, target);
    }

    protected emitEvent(event: event_base) {
        cEvent.emitEvent(event);
    }

    /*初始设置*/
    protected loadConfig() {
        
        this.addAlignMode();
        this.addShowMode();
    }

    private addBlockInput() {
        if (this.blockInputEvent === BlockInput.on) {
            this.node.findComponent(cc.BlockInputEvents);
        }
    }

    private addAlignMode() {
        let mode = cc.Widget.AlignMode.ON_WINDOW_RESIZE;
        switch (this.alignMode) {
            case AlignMode.none:
                break;
            case AlignMode.center:
                UITools.alignCenter(this.node, mode);
                break;
            case AlignMode.full:
                UITools.alignFullScreen(this.node, mode);
                break;
            case AlignMode.left:
                UITools.alignLeft(this.node, mode);
                break;
            case AlignMode.right:
                UITools.alignRight(this.node, mode);
                break;
            case AlignMode.top:
                UITools.alignTop(this.node, mode);
                break;
            case AlignMode.bottom:
                UITools.alignBottom(this.node, mode);
                break;
        }
    };

    addShowMode() {
        let bg_node = this.node.$('bg') || this.node.$('_bg');
        if (!bg_node) {
            return;
        }
        switch (this.showMode) {
            case ShowMode.none:
                //todo
                break;
            case ShowMode.random:
                //todo
                break;
            case ShowMode.easingFade:
                UITools.easingFade(bg_node);
                break;
            case ShowMode.easingFall:
                UITools.easingFall(bg_node);
                break;
            case ShowMode.easingRise:
                UITools.easingRise(bg_node);
                break;
            case ShowMode.easingScale:
                UITools.easingScale(bg_node);
                break;
        }
    }

    async timer(delay: number): Promise<boolean> {
        return new Promise<any>((resolve, reject) => {
            this.scheduleOnce(() => {
                resolve(true);
            }, delay);
        })
    }

}