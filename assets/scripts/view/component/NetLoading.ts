import {NetLoadingView} from '../base/ViewSet';
import {cUI} from '../../config/Config';

const {ccclass, property, menu} = cc._decorator;
@ccclass
@menu('component/NetLoading')
export default class NetLoading extends NetLoadingView {
    is_reconnect = false;

    init(title: string, is_reconnect: boolean) {
        this.is_reconnect = is_reconnect;
        this._tips_title.setLabel(title);
        this.showAction();
    }

    protected showAction() {
        this._tips_img.stopAllActions();
        this.unscheduleAllCallbacks();
        /*非重连*/
        if(!this.is_reconnect) {
            this._tips_img.angle = 0;
            cc.tween(this._tips_img)
                .by(10, {angle: -360})
                .start();
            this.timer(10).then(value => {
                this.node.hide();
            });
        } else {
            cc.tween(this._tips_img)
                .by(10, {angle: -360})
                .repeatForever()
                .start();
        }
    }
}