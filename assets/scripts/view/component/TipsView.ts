import TipsItem from './TipsItem';
import SingleBase from '../../common/base/SingleBase';
import color = cc.color;

const {ccclass, property} = cc._decorator;
@ccclass
export default class TipsView extends SingleBase {
    _time = 0;
    _itemsOffsetY = 0;                   //TipsViewItems队列需要移动的距离
    _itemsY = 0;                         //TipsViewItems队列已经移动的距离
    _currentItem: TipsItem = null;                 //当前新添加的item
    _items: TipsItem[] = [];                   //当前正在显示的items
    _removingItems: TipsItem[] = [];
    private id: number;
    is_enabled: boolean = true;
    max_item_count = 10;
    //即将退出的items
    private pushItem (parent: cc.Node, newItem: TipsItem) {
        var node = newItem.getView();
        parent.addChild(node);

        var old = this._currentItem, item, offset;

        //设置待移出列表新的位移
        if(old)
        {
            this.showItem(old);

            offset = newItem.getView().height / 2 + 1;
            offset += old.getView().height / 2;
            this._itemsY = 0;
            this._itemsOffsetY = offset;
        }
        else if(this._items.length > 0)
        {
            offset = newItem.getView().height / 2 + 1;
            offset += this._items[this._items.length - 1].getView().height / 2;
            this._itemsY = 0;
            this._itemsOffsetY = offset;
        }
        else
        {
            this._itemsY = this._itemsOffsetY = 0;
        }
        this._currentItem = newItem;

        //待移除队列最多显示TipsView, 移除多余的TipsViews
        if(this._items.length > this.max_item_count)
        {
            var i;
            for(i = 0; i < this._removingItems.length; i++)
            {
                item = this._removingItems[i];
                node = item.getView();
                node.destroy();
            }
            this._removingItems = [];

            for(i = 0; i < this._items.length - 1 - this.max_item_count; i++)
            {
                item = this._items[i];
                node = item.getView();
                node.destroy();
                this._items.splice(i,1);
            }
        }
    }

    private showItem (item: TipsItem) {
        if(item == null) return;

        //调整前面被替换的所有TipsView的位置
        var offset = this._itemsOffsetY - this._itemsY;
        this.adjust(offset);

        item.setShow();
        this._items.push(item);
    }

    private adjust (dy: number) {
        if(dy > 0 && this._items.length > 0)
        {
            var i, item;
            for(i = 0; i < this._items.length; i++)
            {
                item = this._items[i];
                item.addOffsetY(dy);
            }

            for(i = 0; i < this._removingItems.length; i++)
            {
                item = this._removingItems[i];
                if(item) {
                    item.addOffsetY(dy);
                }
            }
        }
    }

    // called every frame, uncomment this function to activate update callback
    private update (tick) {
        if(this._currentItem)
        {
            this._currentItem.update(tick);
        }

        //调整位置
        if(this._itemsY < this._itemsOffsetY)
        {
            var d = 5;
            if(this._itemsY + d > this._itemsOffsetY)
            {
                d = this._itemsOffsetY - this._itemsY;
                this._itemsY = this._itemsOffsetY;
            }
            else
            {
                this._itemsY += d;
            }

            this.adjust(d);
        }
        else
        {
            //tips view item队列达到位置后把当前tips推入队列中
            if(this._currentItem && this._currentItem.showExpired())
            {
                this.showItem(this._currentItem);
                this._currentItem = null;
            }
        }

        var i;

        //每秒钟移除一条tips
        if(this._items.length > 0)
        {
            this._time += tick;
            if(this._time >= 1 || this._removingItems.length == 0)
            {
                var item = this._items[0];
                item.setRemoving();
                this._items.splice(0,1);
                this._removingItems.push(item);
                this._time = 0;
            }
        } else {
            this.is_enabled = true;
        }

        var length = this._removingItems.length;
        for(i = length - 1; i >= 0; i--)
        {
            var item = this._removingItems[i];
            item.update(tick);
            if(item.dead())
            {
                var node = item.getView();
                node.destroy();
                this._removingItems.splice(i,1);
            }
        }
    }

    show (parent: cc.Node, node: cc.Node, text: string | number, time: number, color: cc.Color) {

        if (!this.is_enabled) {
            node.destroy();
            return;
        }
        node.x = 0;
        node.y = 100;
        node.zIndex = 999;
        node.show();
        if (this._items.length >= this.max_item_count) {
            this.is_enabled = false;
            text = '操作过于频繁,请稍后再试!';
        }
        let item = node.$(TipsItem);
        this.pushItem(parent, item);
        item._content.color = color || cc.Color.WHITE;
        item.setTipsText(text);
        if (this.id) {
            clearInterval(this.id);
        }
        this.id = setInterval(()=> {
            this.update(0.03);
            if (parent.childrenCount <= 0) {
                clearInterval(this.id);
            }
        }, time);

    }

    clear () {
        this._items.forEach(value => {
            value.node.destroy()
        });
        if(this._currentItem) {
            this._currentItem.node.destroy()
        }
        this._removingItems.forEach(value => {
            value.node.destroy();
        });
        this._currentItem = null;
        this._items = [];
        this._removingItems = [];
    }
}
