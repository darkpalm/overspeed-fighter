
import { $, cUI } from "../../config/Config";
import { data_manager } from "../../dataUtils/DataManeger";
import { Storage } from "../storage/StorageManager";
import { PlayerData, ServerData } from "./PlayerDataConfig";

class PlayerDataMager {
    private static _instance: PlayerDataMager = null;
    static get instance() {
        if (!this._instance) {
            this._instance = new PlayerDataMager();
        }
        return this._instance;
    }

    constructor() {

    }

    public storage = Storage;

    rankNameArr = [
        "见习·机长",
        "实习·机长",
        "初级·机长",
        "中级·机长",
        "高级·机长",
        "顶级·机长",
        "天才·机长",
        "英雄·机长",
        "荣誉·机长",
        "传奇·机长",
        "初级·舰长",
        "中级·舰长",
        "高级·舰长",
        "英雄·舰长",
        "荣誉·舰长",
        "传奇·舰长",
        "神圣·舰长",
    ]
    rankScoreArr = [-1, 1000, 1300, 1600, 2000, 2500, 3000, 3500, 5000, 6000, 8000, 10000, 12500, 15000, 20000, 30000, 50000]

    attArr = ["_hurt", "_speed", "_HP", "_lucky", "_shoot_range", "_bullet_cd", "_bullet_speed", "_penetrate_count"];
    attValueArr = [0.5, 0.5, 2, 0.1, 120, 50, 0.25, 1];


    playerData: PlayerData = {
        playerId: "",
        openId: "",//玩家玩家唯一标识
        nickName: "超速战机",//玩家昵称
        avatarUrl: '',//玩家头像路径
        killRivalCount: 0,//击杀敌人总数
        maxLifeDay: 0,//单局游戏最大存活周期
        maxKillCount: 0,//单局游戏最高击杀数
        attLv: new Array(8).fill(0),//强化属性的等级
        xinPianSuiPian: 0,
        actionTime: 120,
        isFollowed: false,
        rank: "见习机长",

        base_lucky: 0.2,
        base_HP: 10,
        base_speed: 2.5,
        base_bullet_cd: 350,
        base_bullet_speed: 0.5,
        base_penetrate_count: 0,
        base_volume: 1,
        base_shoot_count: 1,
        base_bullet_volume: 1,
        base_shoot_range: 400,
        base_frozen: 0,
        base_life_stealing: 0,
        base_hurt: 1,
    }


    //初始化数据
    initData() {
        this.storage.init("cszj", "chaosuzhanji");
        let data = this.storage.get("cszj");
        // console.log('--------------data:', data);
        if (data) {
            this.updatePlayerData(JSON.parse(data));
        }
        this.playerData.rank = this.getRankName();
        // this.playerData = this.storage.get("yinglingduijue", saveData);
        // console.log('--------------this.playerData  initData:', this.playerData);
    }

    /***
     * 更新用户数据
     */
    updatePlayerData(data: ServerData) {
        for (let key in data) {
            this.playerData[key] = data[key];
        }
        // console.log('--------------this.playerData:', this.playerData, data);
    }

    /**
     * 保存用户数据
     */
    saveData() {
        let saveData: ServerData = this.initServerData();
        this.storage.set("cszj", JSON.stringify(saveData));
    }

    /**
     * 整理需要保存的数据
     * @returns 
     */
    initServerData(): ServerData {
        let saveData: ServerData = {
            openId: this.playerData.openId,//玩家玩家唯一标识
            nickName: this.playerData.nickName,//玩家昵称
            avatarUrl: this.playerData.avatarUrl,//玩家头像路径
            killRivalCount: this.playerData.killRivalCount,//进行的总对局数
            maxLifeDay: this.playerData.maxLifeDay,//胜利的局数
            maxKillCount: this.playerData.maxKillCount,//勾玉数量
            attLv: this.playerData.attLv,//抽牌堆记录
            xinPianSuiPian: this.playerData.xinPianSuiPian,
        };
        return saveData;
    }

    /**
     * 获取基础属性的加持
     * @param index 
     * @returns 
     */
    getAddAttValue(index: number, addLv: number = 0) {
        let lv = this.playerData.attLv[index] + addLv;
        let baseValue = this.playerData["base" + this.attArr[index]];
        let value = baseValue + lv * this.attValueArr[index];
        if (index == 7) {
            value = baseValue + lv;
        } else if (index == 5) {
            value = baseValue - lv * this.attValueArr[index];
        }
        value = Math.floor(value * 10) / 10;
        return value;
    }

    /**
     * 获取战力
     */
    getZhanLi(){
        let value = 0;
        for(let i = 0; i < this.attArr.length; i++){
            value += this.getAddAttValue(i);
        }
        value = this.playerData.killRivalCount + value + this.playerData.maxLifeDay*50;
        return value;
    }

    /**
     * 获取称号
     */
    getRankName(){
        let value = this.getZhanLi();
        let index = 0;
        for(let i=0; i < this.rankScoreArr.length; i++){
            if(value < this.rankScoreArr[i]){
                index = i;
                break;
            }
        }
        return this.rankNameArr[index];
    }

    /**
     * 获取段位下标
     */
     getRankIndex(){
        let value = this.getZhanLi();
        let index = 0;
        for(let i=0; i < this.rankScoreArr.length; i++){
            if(value < this.rankScoreArr[i]){
                index = i;
                break;
            }
        }
        return index;
    }

}

export var p_data_mager = PlayerDataMager.instance;
