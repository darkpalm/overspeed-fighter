import SingleBase from "../../common/base/SingleBase";
import { p_data_mager } from "../playerDataMager/PlayerDataMager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameDataMager extends SingleBase {
    startRoundTime: number = 0;//回合开始时的时间戳 - 毫秒
    actionTimer: number = 60;//当前回合剩余的时间
    roundTimes: number = 0;//当前游戏的大回合数
    firstPlayerId: string = null;//先手的玩家id
    roundPlayerId: string = "";//当前回合的玩家id

    initData(){

    }

   
}
