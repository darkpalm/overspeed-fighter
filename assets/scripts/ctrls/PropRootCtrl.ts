import { $, cNodePool, cUI } from "../config/Config";
import Xingxing from "../items/props/Xingxing";
import { gameScene } from "../view/scene/GameScene";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PropRootCtrl extends cc.Component {

    // onLoad () {}

    start() {

    }

    /**
     * 掉落道具
     * @param key 掉落物品标识
     * @param propPos 掉落位置
     */
    propItem(key: string, propPos: cc.Vec2) {

    }

    propXingXing(propPos: cc.Vec2) {
        cNodePool.get($.Prefab.xingxing).then(value => {
            value.parent = this.node;
            value.getComponent(Xingxing).initPos(propPos);
        })
    }

    /**
     * 场景中的星星飞向玩家
     */
    itemsMovePlayer() {
        gameScene.pauseGame();
        this.judgeFinish();
        for (let i = 0; i < this.node.childrenCount; i++) {
            if (this.node.children[i].name == "xingxing") this.node.children[i].getComponent(Xingxing).move();
        }
    }

    /**
     * 判断是否接收完所有星星
     */
    judgeFinish() {
        if (!this.node.getChildByName("xingxing")) {
            cUI.addLayer($.Prefab.shopNode);
        }
    }

    // update (dt) {}
}
