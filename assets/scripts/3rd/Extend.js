/*cc.Node*/
/*当前只有button widget 设置自动添加*/

cc.Node.prototype.findComponent = function (type) {
    return this.getComponent(type) || this.addComponent(type);
};
cc.Node.prototype.$ = function (target, inChildrenSearch = false) {
    /*节点*/
    if (typeof target === 'string') {
        return cc.find(target, this);
    }
    /*组件*/
    if (!inChildrenSearch) {
        return this.getComponent(target);
    }
    return this.getComponentInChildren(target);
};
cc.Node.prototype.hideChildren = function () {
    this.children.forEach(node => node.hide());
};
cc.Node.prototype.show = function () {
    this.active = true;
};
cc.Node.prototype.hide = function () {
    this.active = false;
};
cc.Node.prototype.check = function () {
    this.isChecked = true;
};
cc.Node.prototype.uncheck = function () {
    this.isChecked = false;
};
cc.Node.prototype.getPhyCollider = function () {
    return this.$(cc.PhysicsBoxCollider) || this.$(cc.PhysicsCircleCollider) || this.$(cc.PhysicsPolygonCollider);
};
cc.Node.prototype.destroyAllChildrens = function(type) {
   this.getComponentsInChildren(type).forEach(value => value.node.destroy());
};
cc.Node.prototype.toggle = function () {
    this.active = !this.active;
};
cc.Node.prototype.setLabel = function (str) {
    let label = this.getComponent(cc.Label);
    label.string = str.toString();
};
cc.Node.prototype.setText = function (str) {
    let label = this.getComponent(cc.RichText);
    label.string = str.toString();
};
cc.Node.prototype.setSpriteFrame = function (spriteFrame) {
    let sprite = this.getComponent(cc.Sprite);
    sprite.spriteFrame = spriteFrame;
};
cc.Node.prototype.setProgress = function (percent) {
    let progressBar = this.getComponent(cc.ProgressBar);
    progressBar.progress = percent;
};
cc.Node.prototype.click = function (callback = null, btn_effect = 0) {
    this.on('touchend', function (e) {
        if (callback) {
            callback(e);
        }
    });
    this.findComponent(cc.Button).transition = btn_effect ? cc.Button.Transition.SCALE : cc.Button.Transition.COLOR;
};

Object.defineProperty(Array.prototype, 'last', {
    get() {
        return this[this.length - 1];
    }
});
Array.prototype.init = function(count, value) {

    while (count > 0) {
        this[--count] = value >= 0 ? value : count;
    }
    return this;
};
String.prototype.toNumber = function () {
    return +this;
};
String.prototype.toFormat = function (num) {
    return this.replace(/\d+$/, num + '');
};
String.prototype.format = function (num) {
    return this.replace(/\d/, num + '');
};
/*end*/

/*cc.Animation*/
cc.Animation.prototype.fadeIn = function (name, fadeInTime, playTimes) {
    this.playAdditive(name, 0);
};

cc.Animation.prototype.reset = function () {
    this.setCurrentTime(0);
    // this.stop();
};

cc.Animation.prototype.gotoAndStopByFrame = function (name, frame) {
    this.setCurrentTime(frame, name);
    // this.stop();
};

cc.Animation.prototype.getState = function (name) {
    let isPlaying = this.getAnimationState(name);
    return {isPlaying};
};
/*end*/
cc.Tween.quadOut = 'quadOut';
cc.Tween.quadInOut = 'quadInOut';
cc.Tween.cubicIn = 'cubicIn';
cc.Tween.cubicOut = 'cubicOut';
cc.Tween.cubicInOut = 'cubicInOut';
cc.Tween.quartIn = 'quartIn';
cc.Tween.quartOut = 'quartOut';
cc.Tween.quartInOut = 'quartInOut';
cc.Tween.quintIn = 'quintIn';
cc.Tween.quintOut = 'quintOut';
cc.Tween.quintInOut = 'quintInOut';
cc.Tween.sineIn = 'sineIn';
cc.Tween.sineOut = 'sineOut';
cc.Tween.sineInOut = 'sineInOut';
cc.Tween.expoIn = 'expoIn';
cc.Tween.expoOut = 'expoOut';
cc.Tween.expoInOut = 'expoInOut';
cc.Tween.circIn = 'circIn';
cc.Tween.circOut = 'circOut';
cc.Tween.circInOut = 'circInOut';
cc.Tween.elasticIn = 'elasticIn';
cc.Tween.elasticOut = 'elasticOut';
cc.Tween.elasticInOut = 'elasticInOut';
cc.Tween.backIn = 'backIn';
cc.Tween.backOut = 'backOut';
cc.Tween.backInOut = 'backInOut';
cc.Tween.bounceIn = 'bounceIn';
cc.Tween.bounceOut = 'bounceOut';
cc.Tween.bounceInOut = 'bounceInOut';
cc.Tween.smooth = 'smooth';
cc.Tween.fade = 'fade';
