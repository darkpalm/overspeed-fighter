import Utils from "../common/utils/Utils";
import { skill_data_text } from "./DataClass";
class DataManeger {
    //单例
    private static _instace: DataManeger;
    /** 单例的必要方法 */
    public static get Instance(): DataManeger {
        if (!this._instace)
            this._instace = new DataManeger;
        return this._instace;
    };

    /**升级属性的数值数据 */
    public skill_data: skill_data_text[] = [];
   

    getSkillDataByKey(key: string): skill_data_text{
        for(let i = 0; i < this.skill_data.length; i++){
            if(this.skill_data[i].add_key == key){
                return this.skill_data[i];
            }
        }
        return;
    }
}
export let data_manager = DataManeger.Instance;